﻿Public Class Form1

    Public ShopItems As ArrayList = New ArrayList()
    Public PriceArray As ArrayList = New ArrayList(0)

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ShopItems.Add(New ShopItem(TextBox1.Text, Val(TextBox2.Text), TextBox3.Text))
        ListBox1.Items.Add(String.Format("{0} - {1}", ShopItems(ShopItems.Count - 1).Name, ShopItems(ShopItems.Count - 1).Price))
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim result As DialogResult = OpenFileDialog1.ShowDialog
        If result = Windows.Forms.DialogResult.OK Then
            TextBox3.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        PictureBox1.Image = Image.FromFile(ShopItems(ListBox1.SelectedIndex).PicPath)
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ListBox2.Items.Add(ListBox1.SelectedItem)
        PriceArray.Add(ShopItems(ListBox1.SelectedIndex).Price)
        Label4.Text = String.Format("Price: {0}", Total(PriceArray))
    End Sub

    Private Sub ListBox1_DoubleClick(sender As Object, e As EventArgs) Handles ListBox1.DoubleClick
        ListBox2.Items.Add(ListBox1.SelectedItem)
        PriceArray.Add(ShopItems(ListBox1.SelectedIndex).Price)
        Label4.Text = String.Format("Price: {0}", Total(PriceArray))
    End Sub

    Private Sub ListBox2_DoubleClick(sender As Object, e As EventArgs) Handles ListBox2.DoubleClick
        ListBox2.Items.Remove(ListBox2.SelectedItem)
        PriceArray.RemoveAt(ListBox2.SelectedIndex + 1)
        If PriceArray.Count = 0 Then
            Label4.Text = "Price: 0"
        Else
            Label4.Text = String.Format("Price: {0}", Total(PriceArray))
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        ListBox2.Items.Remove(ListBox2.SelectedItem)
        PriceArray.RemoveAt(ListBox2.SelectedIndex + 1)
        If PriceArray.Count = 0 Then
            Label4.Text = "Price: 0"
        Else
            Label4.Text = String.Format("Price: {0}", Total(PriceArray))
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        MessageBox.Show(String.Format("Your total payment is {0}, Thank you for shopping with us.", Total(PriceArray)))
        ListBox2.Items.Clear()
        PriceArray.Clear()
        PriceArray.Add(0)
        Label4.Text = String.Format("Price: {0}", Total(PriceArray))
    End Sub
End Class