﻿Public Class ShopItem

    Public Name As String
    Public Price As Double
    Public PicPath As String

    Sub New(ByVal Name As String, ByVal Price As Double, ByVal PicPath As String)
        Me.Name = Name
        Me.Price = Price
        Me.PicPath = PicPath
    End Sub

End Class
